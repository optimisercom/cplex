/*********************************************
 * OPL 12.8.0.0 Model
 * Author: kaiph
 * Creation Date: May 22, 2019 at 4:16:29 PM
 *********************************************/

 // variables
 
 dvar int+ x;
 dvar int+ y;
 
 // expressions
 
 dexpr int cost = 500*x + 400*y;
 
 // model
 
 minimize cost;
 subject to {
  
 	 cond01:
 	 40*x + 30*y >= 300;
 }
 
 // postprocessing

 execute {
 	if(cplex.getCplexStatus()==1) {
 		writeln("reduced cost of x ", x.reducedCost);
 		writeln("reduced cost of y ", y.reducedCost);
 		writeln("primal of cond01 ", cond01.primal);
 	} else {
 		writeln("ERROR: solution not found");
 	}
 }