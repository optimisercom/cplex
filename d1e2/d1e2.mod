/*********************************************
 * OPL 12.8.0.0 Model
 * Author: kaiph
 * Creation Date: May 26, 2019 at 11:21:59 PM
 * Fixed data without reading Excel
 *********************************************/
 
// parameters

int n = 4; // number of cargos
int m = 3; // number of compartments

range cargos = 1..n;
range compartments = 1..m;

float profit[cargos] = [310,380,350,285]; // d1e2.dat > profit from SheetRead(my_sheet, "'Sheet1':H2:H5");
float weight[cargos] = [18,15,23,12];
float volume[cargos] = [480,650,580,390];

float weight_cap[compartments] = [10,16,8];
float space_cap[compartments] = [6800,8700,5300];

// variables

dvar float+ x[cargos][compartments];
dvar float+ y;

maximize sum(i in cargos, j in compartments) profit[i]*x[i][j];

subject to {
	// Available weight i
	forall(i in cargos)
	  available_weight:
	  sum(j in compartments) x[i][j] <= weight[i];
	  
	// Capacity of compartment j  
	forall(j in compartments)
	  weight_capacity:
	  sum(i in cargos) x[i][j] <= weight_cap[j];
	  
	// Space capacity of compartment  
	forall(j in compartments)
	  space_capacity:
	  sum(i in cargos) volume[i]*x[i][j] <= space_cap[j];
	  
	// Volume of cargo i
	forall(j in compartments)
	  balanced_plane:
	  sum(i in cargos) x[i][j] / weight_cap[j] == y;
}
