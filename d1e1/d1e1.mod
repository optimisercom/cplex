/*********************************************
 * OPL 12.8.0.0 Model
 * Author: kaiph
 * Creation Date: May 22, 2019 at 4:16:29 PM
 *********************************************/

 // variables
 
 dvar float+ x; // + --> real number
 dvar float+ y;
 
 // expressions
 
 dexpr float cost = 0.12*x + 0.15*y;
 
 // model
 
 minimize cost;
 subject to {
  
 	 cond01: // name of the constraint 1
 	 60*x + 60*y >= 300;
 	 
 	 cond02:
 	 12*x + 6*y >= 36;
 	 
 	 cond03:
 	 10*x + 30*y >= 90;
 }
 
 // postprocessing

 execute {
 	if(cplex.getCplexStatus()==1) {
 		writeln("reduced cost of x ", x.reducedCost);
 		writeln("reduced cost of y ", y.reducedCost);
 		writeln("dual of cond01 ", cond01.dual);
 		writeln("dual of cond02 ", cond02.dual);
 		writeln("dual of cond03 ", cond03.dual);
 	} else {
 		writeln("ERROR: solution not found");
 	}
 }